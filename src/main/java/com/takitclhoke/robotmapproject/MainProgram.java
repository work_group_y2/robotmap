/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.robotmapproject;

import java.util.Scanner;

/**
 *
 * @author ทักช์ติโชค
 */
public class MainProgram {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        TableMap map = new TableMap(10, 10);
        Robot robot = new Robot(2, 2, 'x', map);
        Bomb bomb = new Bomb(5, 5);
        map.setRobot(robot);
        map.setBomb(bomb);
        while (true) {
            map.showMap();
            // W,a|N,w|E,d|S,s|Q:quit
            String str = kb.next();
            char direction = str.charAt(0);
            if (direction == 'q') {
                System.out.println("Bye Byee!!");
                break;
            }
            robot.walk(direction);

        }
    }

}
